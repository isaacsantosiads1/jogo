# Jogo

Este é um projeto de um jogo baseado em turnos

# Executar

1. Baixar o projeto api `https://gitlab.com/isaacsantosiads1/jogo-api`;
2. Clonar este projeto;
3. Ir para o diretorio do projeto clonado;
4. Baixar as dependencias com 'npm install';
5. Executar a aplicação com 'ng serve'.
