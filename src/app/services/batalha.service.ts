import { Injectable } from '@angular/core';
import { JogadorService } from './jogador.service';
import { MonstroService } from './monstro.service';
import { LogService } from './log.service';

export interface Resultado {
    titulo: string,
    pontuacao: number
}

@Injectable({
    providedIn: 'root'
})
export class BatalhaService {

    private desistencia: boolean;
    private resultado: Resultado;
    private turno: number;

    constructor(
        private jogador: JogadorService,
        private monstro: MonstroService,
        private log: LogService
    ) {
        this.iniciar();
    }

    iniciar() {
        this.turno = 0;
        this.desistencia = false;

        this.jogador.iniciar();
        this.monstro.iniciar();
        this.log.iniciar();
    }

    atacar() {
        this.turno++;

        this.monstro.atacar(this.jogador);
        this.jogador.atacar(this.monstro);
    }

    especial() {
        this.turno++;

        this.monstro.atacar(this.jogador);
        this.jogador.especial(this.monstro);
    }

    curar() {
        this.turno++;

        this.monstro.atacar(this.jogador);
        this.jogador.curar();
    }

    desistir() {
        this.desistencia = true;
    }

    getJogador() {
        return this.jogador.personagem;
    }

    getMonstro() {
        return this.monstro.personagem;
    }

    isCarregandoEspecial() {
        return this.jogador.isCarregandoEspecial();
    }

    isFinalizada() {
        return this.desistencia ||
            this.getJogador().vida == 0 ||
            this.getMonstro().vida == 0;
    }

    isVitoriaJogador() {
        return !this.desistencia && this.getMonstro().vida == 0;
    }

    getTitulo() {
        let titulo = "";

        if (this.desistencia) {
            titulo = "Você desistiu da batalha";
        } else if (this.getJogador().vida == 0) {
            titulo = "Você perdeu a batalha";
        } else if (this.getMonstro().vida == 0) {
            titulo = "Você venceu a batalha";
        }

        return titulo;
    }

    getPontuacao() {
        return (this.getJogador().vida * 1000) / this.turno;
    }
}
