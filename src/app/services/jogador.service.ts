import { Injectable } from '@angular/core';
import { Personagem } from '../interfaces/personagem';
import { MonstroService } from './monstro.service';
import { RandomService } from './random.service';
import { LogService } from './log.service';

@Injectable({
    providedIn: 'root'
})
export class JogadorService {

    personagem: Personagem;

    constructor(
        private random: RandomService,
        private log: LogService) {
    }

    iniciar() {
        this.personagem = {
            tipo: 'Jogador',
            vida: 100,
            ataque: {
                min: 5,
                max: 8
            },
            special: {
                min: 7,
                max: 11,
                turnos: 0
            },
            cura: {
                min: 5,
                max: 10
            },
            tonto: false
        };
    }

    atacar(monstro: MonstroService) {
        if (this.personagem.vida > 0) {
            let dano = this.random.numero(
                this.personagem.ataque.min,
                this.personagem.ataque.max
            );
            this.log.addJogadorAtaque("Jogador atacou o monstro (-" + dano + ")");
            monstro.dano(dano);

            this.carregarEspecial();
        }
    }

    especial(monstro: MonstroService) {
        if (this.personagem.vida > 0) {
            let dano = this.random.numero(
                this.personagem.special.min,
                this.personagem.special.max
            );
            let ficarTonto = this.random.numero(0, 100) <= 40;
            let realizarCura = this.random.numero(0, 100) <= 25;

            this.log.addJogadorEspecial("Jogador usou ataque especial no monstro (-" + dano + ")");
            monstro.dano(dano);
            monstro.tonto(ficarTonto);

            if (realizarCura) {
                this.curar(Math.floor(dano / 2));
            }

            this.personagem.special.turnos = 2;
        }
    }

    curar(pontos?: number) {
        if (this.personagem.vida > 0) {
            let pontosPersonagem = this.personagem.vida;

            if (!pontos) {
                pontos = this.random.numero(
                    this.personagem.cura.min,
                    this.personagem.cura.max
                );
            }

            pontosPersonagem += pontos;
            if (pontosPersonagem > 100) {
                pontosPersonagem = 100;
            }

            this.log.addJogadorCura("Jogador usou cura (+" + pontos + ")");
            this.personagem.vida = pontosPersonagem;

            this.carregarEspecial();
        }
    }

    dano(valor: number) {
        let vida = this.personagem.vida - valor;

        if (vida < 0) {
            vida = 0;
        }

        this.personagem.vida = vida;
    }

    isCarregandoEspecial() {
        return this.personagem.special.turnos > 0;
    }

    carregarEspecial() {
        if (this.isCarregandoEspecial) {
            this.personagem.special.turnos -= 1;
        }
    }
}
