import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RankingService {

    constructor(private http: HttpClient) { }

    getJogadores(): any {
        return this.http.get('http://localhost:3000/jogadores');
    }

    addJogador(nome: string, pontuacao: number): any {
        return this.http.post(
            'http://localhost:3000/jogadores',
            {
                nome: nome,
                pontuacao: pontuacao
            },
            {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json'
                })
            }
        );
    }
}
