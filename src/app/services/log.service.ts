import { Injectable } from '@angular/core';

interface Acao {
    tipo: string,
    mensagem: string
}

@Injectable({
    providedIn: 'root'
})
export class LogService {

    acoes: Array<Acao>;

    constructor() {
    }

    private add(acao: Acao) {
        this.acoes.push(acao);
    }

    iniciar() {
        this.acoes = [];
    }

    addMonstroAtaque(mensagem: string) {
        let acao = {
            tipo: 'monstro_ataque',
            mensagem: mensagem
        }
        this.add(acao);
    }

    addMonstroTonto(mensagem: string) {
        let acao = {
            tipo: 'monstro_ataque',
            mensagem: mensagem
        }
        this.add(acao);
    }

    addJogadorAtaque(mensagem: string) {
        let acao = {
            tipo: 'jogador_ataque',
            mensagem: mensagem
        }
        this.add(acao);
    }

    addJogadorEspecial(mensagem: string) {
        let acao = {
            tipo: 'jogador_especial',
            mensagem: mensagem
        }
        this.add(acao);
    }

    addJogadorCura(mensagem: string) {
        let acao = {
            tipo: 'jogador_cura',
            mensagem: mensagem
        }
        this.add(acao);
    }
}
