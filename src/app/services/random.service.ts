import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class RandomService {

    constructor() { }

    numero(minimo: number, maximo: number) {
        return Math.floor(Math.random() * (maximo - minimo + 1)) + minimo;
    }
}
