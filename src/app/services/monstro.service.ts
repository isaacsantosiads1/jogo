import { Injectable } from '@angular/core';
import { Personagem } from '../interfaces/personagem';
import { JogadorService } from './jogador.service';
import { RandomService } from './random.service';
import { LogService } from './log.service';

@Injectable({
    providedIn: 'root'
})
export class MonstroService {

    personagem: Personagem;

    constructor(
        private random: RandomService,
        private log: LogService
    ) { }

    iniciar() {
        this.personagem = {
            tipo: 'Monstro',
            vida: 100,
            ataque: {
                min: 6,
                max: 12
            },
            tonto: false
        };
    }

    atacar(jogador: JogadorService) {
        if (!this.personagem.tonto) {
            let dano = this.random.numero(
                this.personagem.ataque.min,
                this.personagem.ataque.max
            );

            jogador.dano(dano);
            this.log.addMonstroAtaque("Monstro causou dano (-" + dano + ")");
        } else {
            this.log.addMonstroTonto("Monstro não pode atacar neste turno");
        }

        this.personagem.tonto = false;
    }

    dano(valor: number) {
        let vida = this.personagem.vida - valor;

        if (vida < 0) {
            vida = 0;
        }

        this.personagem.vida = vida;
    }

    tonto(status: boolean) {
        this.personagem.tonto = status;
    }
}
