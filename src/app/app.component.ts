import { Component } from '@angular/core';
import { Personagem } from './interfaces/personagem';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'jogo';

    personagem: Personagem;

    constructor() {
        this.personagem = {
            tipo: "jogador",
            vida: 100,
            ataque: {
                min: 5,
                max: 10
            }
        };
    }

    diminuir() {
        this.personagem.vida -= 10;
    }

    aumentar() {
        this.personagem.vida += 10;
    }
}
