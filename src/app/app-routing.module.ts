import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JogoComponent } from './paginas/jogo/jogo.component';
import { HomeComponent } from './paginas/home/home.component';
import { RankingComponent } from './paginas/ranking/ranking.component';
import { IniciarJogoComponent } from './paginas/iniciar-jogo/iniciar-jogo.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'ranking', component: RankingComponent },
    { path: 'iniciar-jogo', component: IniciarJogoComponent },
    { path: 'jogo', component: JogoComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
