export interface Personagem {
    tipo: string,
    vida: number,
    ataque: {
        min: number,
        max: number
    },
    special?: {
        min: number,
        max: number,
        turnos: number
    },
    cura?: {
        min: number,
        max: number
    },
    tonto?: boolean
}
