import { Component, OnInit, Input } from '@angular/core';
import { Personagem } from 'src/app/interfaces/personagem';

@Component({
    selector: 'barra-vida',
    templateUrl: './barra-vida.component.html',
    styleUrls: ['./barra-vida.component.css']
})
export class BarraVidaComponent implements OnInit {

    @Input()
    personagem: Personagem;

    constructor() { }

    ngOnInit() {
    }

    getClassColor() {
        let classe = "bg-success";

        if (this.getPorcentagemVida() < 20) {
            classe = "bg-danger";
        }

        return classe;
    }

    getPorcentagemVida() {
        let maximo = 100;
        return (this.personagem.vida * 100) / maximo;
    }
}
