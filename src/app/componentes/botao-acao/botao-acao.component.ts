import { Component, OnInit, Input } from '@angular/core';
import { isListLikeIterable } from '@angular/core/src/change_detection/change_detection_util';

@Component({
    selector: 'botao-acao',
    templateUrl: './botao-acao.component.html',
    styleUrls: ['./botao-acao.component.css']
})
export class BotaoAcaoComponent implements OnInit {

    @Input()
    tipo: string = "button";

    @Input()
    estilo: string;

    @Input()
    desabilitado: boolean = false;

    constructor() { }

    ngOnInit() { }
}
