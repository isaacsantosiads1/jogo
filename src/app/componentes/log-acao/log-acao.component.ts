import { Component, OnInit } from '@angular/core';
import { LogService } from 'src/app/services/log.service';

@Component({
    selector: 'log-acao',
    templateUrl: './log-acao.component.html',
    styleUrls: ['./log-acao.component.css']
})
export class LogAcaoComponent implements OnInit {

    constructor(private log: LogService) { }

    ngOnInit() {
    }

    linhaTempo() {
        let acoes = this.log.acoes;
        acoes.reverse();
        return acoes;
    }

    getClasse(tipo: string) {
        let classe = "";

        switch (tipo) {
            case 'monstro_ataque':
                classe = "bg-danger text-right"
                break;
            case 'monstro_tonto':
                classe = "bg-dark text-right";
                break;
            case 'jogador_ataque':
                classe = "bg-primary text-left";
                break;
            case 'jogador_especial':
                classe = "bg-warning text-left";
                break;
            case 'jogador_cura':
                classe = "bg-success text-left";
                break;
            default:
                break;
        }

        return classe;
    }
}
