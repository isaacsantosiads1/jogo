import { Component, OnInit } from '@angular/core';
import { BatalhaService } from 'src/app/services/batalha.service';
import { RankingService } from 'src/app/services/ranking.service';
import { Router } from '@angular/router';

export interface Vencedor {
    nome: string
}

@Component({
    selector: 'pagina-jogo',
    templateUrl: './jogo.component.html',
    styleUrls: ['./jogo.component.css']
})
export class JogoComponent implements OnInit {

    nome: string;
    response;

    constructor(public batalha: BatalhaService, private api: RankingService, private router: Router) { }

    ngOnInit() {
        this.nome = "";
        this.batalha.iniciar();
    }

    salvarPontuacao() {
        if (this.nome != '') {
            this.api.addJogador(
                this.nome,
                this.batalha.getPontuacao()
            ).subscribe(data => this.response = data.message);

            this.router.navigate(['/ranking']);
        }

        return false;
    }
}
