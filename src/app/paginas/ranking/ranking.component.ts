import { Component, OnInit } from '@angular/core';
import { RankingService } from 'src/app/services/ranking.service';

@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.component.html',
    styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

    jogadores = [];

    constructor(private api: RankingService) { }

    ngOnInit() {
        this.api.getJogadores().subscribe(data => this.jogadores = data);
    }

}
