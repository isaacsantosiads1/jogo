import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BarraVidaComponent } from './componentes/barra-vida/barra-vida.component';
import { BotaoAcaoComponent } from './componentes/botao-acao/botao-acao.component';
import { LogAcaoComponent } from './componentes/log-acao/log-acao.component';
import { JogoComponent } from './paginas/jogo/jogo.component';
import { AppRoutingModule } from './app-routing.module';
import { RankingComponent } from './paginas/ranking/ranking.component';
import { IniciarJogoComponent } from './paginas/iniciar-jogo/iniciar-jogo.component';
import { HomeComponent } from './paginas/home/home.component';

registerLocaleData(localePt, 'pt');

@NgModule({
    declarations: [
        AppComponent,
        BarraVidaComponent,
        BotaoAcaoComponent,
        LogAcaoComponent,
        JogoComponent,
        RankingComponent,
        IniciarJogoComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [{
        provide: LOCALE_ID, useValue: 'pt'
    }],
    bootstrap: [AppComponent]
})
export class AppModule {

}
